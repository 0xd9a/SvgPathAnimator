/*
 * Copyright (C) 2016 Kasun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package kasun.svg.path_animator

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import androidx.annotation.IntDef
import androidx.core.view.ViewCompat

/**
 * Animated SVG Drawing for Android
 */
class SvgPathAnimatorView : View {
    private var mTraceTime = 0
    private lateinit var mTraceResidueColors: IntArray
    private lateinit var mTraceColors: IntArray
    private var mViewportWidth = 64f
    private var mViewportHeight = 64f
    private var mViewport = PointF(mViewportWidth, mViewportHeight)
    private var aspectRatioWidth = 1f
    private var aspectRatioHeight = 1f
    private lateinit var mGlyphData: Array<GlyphData>
    private lateinit var mGlyphStrings: Array<String>
    private var mWidth = 0
    private var mHeight = 0
    private var mStartTime: Long = 0

    /**
     * Set a multiplier for the speed of the animation.
     */
    var speedMultiplier = 1F

    /**
     * Set the width for all paths. Same as strokeWidth attribute in SVG.
     */
    var strokeWidth = 1F

    /**
     * Delay between the end of tracing a path and start tracing the next path.
     * Value should be time in milliseconds.
     * Only valid for glyphs with multiple paths.
     */
    var delayBetweenPaths = 250

    /**
     * Set the color used for drawing paths. Same as 'strokeColor' in svg.
     */
    var strokeColor = Color.WHITE
        set(value) {
            field = value
            if (!this::mGlyphStrings.isInitialized) {
                throw RuntimeException("You need to set the glyphs first.")
            }
            val colors = IntArray(mGlyphStrings.size)
            for (i in mGlyphStrings.indices) {
                colors[i] = value
            }
            setStrokeColors(colors)
        }

    /**
     * Get the animation state.
     *
     * @return Either [STATE_NOT_STARTED],
     * [STATE_TRACE_STARTED] or
     * [STATE_FINISHED]
     */
    @get:State
    var state = STATE_NOT_STARTED
        private set
    private var mOnStateChangeListener: OnStateChangeListener? = null

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        mTraceColors = IntArray(1)
        mTraceColors[0] = Color.BLACK
        mTraceResidueColors = IntArray(1)
        mTraceResidueColors[0] = 0x32000000
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.SvgPathAnimatorView)
            mViewportWidth =
                a.getInt(R.styleable.SvgPathAnimatorView_svgAnimatorImageSizeX, 64).toFloat()
            aspectRatioWidth =
                a.getInt(R.styleable.SvgPathAnimatorView_svgAnimatorImageSizeX, 64).toFloat()
            mViewportHeight =
                a.getInt(R.styleable.SvgPathAnimatorView_svgAnimatorImageSizeY, 64).toFloat()
            aspectRatioHeight =
                a.getInt(R.styleable.SvgPathAnimatorView_svgAnimatorImageSizeY, 64).toFloat()
            mTraceTime = a.getInt(R.styleable.SvgPathAnimatorView_svgAnimatorTraceTime, 2000)
            val glyphStringsId =
                a.getResourceId(R.styleable.SvgPathAnimatorView_svgAnimatorGlyphStrings, 0)
            val strokeColorsId =
                a.getResourceId(R.styleable.SvgPathAnimatorView_svgAnimatorStrokeColors, 0)
            a.recycle()
            if (glyphStringsId != 0) {
                setGlyphStrings(resources.getStringArray(glyphStringsId))
                strokeColor = Color.BLACK
            }
            if (strokeColorsId != 0) {
                setStrokeColors(resources.getIntArray(strokeColorsId))
            }
            mViewport = PointF(mViewportWidth, mViewportHeight)
        }

        // Note: using a software layer here is an optimization. This view works with hardware accelerated rendering but
        // every time a path is modified (when the dash path effect is modified), the graphics pipeline will rasterize
        // the path again in a new texture. Since we are dealing with dozens of paths, it is much more efficient to
        // rasterize the entire view into a single re-usable texture instead. Ideally this should be toggled using a
        // heuristic based on the number and or dimensions of paths to render.
        setLayerType(LAYER_TYPE_SOFTWARE, null)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mWidth = w
        mHeight = h
        rebuildGlyphData()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var width = MeasureSpec.getSize(widthMeasureSpec)
        var height = MeasureSpec.getSize(heightMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        if (height <= 0 && width <= 0 && heightMode == MeasureSpec.UNSPECIFIED && widthMode == MeasureSpec.UNSPECIFIED) {
            width = 0
            height = 0
        } else if (height <= 0 && heightMode == MeasureSpec.UNSPECIFIED) {
            height = (width * aspectRatioHeight / aspectRatioWidth).toInt()
        } else if (width <= 0 && widthMode == MeasureSpec.UNSPECIFIED) {
            width = (height * aspectRatioWidth / aspectRatioHeight).toInt()
        } else if (width * aspectRatioHeight > aspectRatioWidth * height) {
            width = (height * aspectRatioWidth / aspectRatioHeight).toInt()
        } else {
            height = (width * aspectRatioHeight / aspectRatioWidth).toInt()
        }
        super.onMeasure(
            MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
            MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        )
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (state == STATE_NOT_STARTED) return

        val t = System.currentTimeMillis() - mStartTime

        // Draw outlines (starts as traced)
        for (i in mGlyphData.indices) {
            val phase =
                constrain((t - mGlyphData[i].traceStartDelay).toFloat() / mGlyphData[i].traceTime)
            val distance =
                INTERPOLATOR.getInterpolation(phase) * mGlyphData[i].length
            mGlyphData[i].paint!!.color = mTraceResidueColors[i]
            if (t >= mGlyphData[i].traceStartDelay) {
                mGlyphData[i].paint!!.pathEffect =
                    DashPathEffect(floatArrayOf(distance, mGlyphData[i].length), 0F)
                canvas.drawPath(mGlyphData[i].path!!, mGlyphData[i].paint!!)
            }
        }
        if (t < mTraceTime) {
            // draw next frame if animation isn't finished
            ViewCompat.postInvalidateOnAnimation(this)
        } else {
            changeState(STATE_FINISHED)
        }
    }

    private fun calculateTraceTime(length: Float): Int {
        return try {
            val x = mWidth / 32F
            val y = mHeight / 32F
            val scaleMatrix = Matrix()
            scaleMatrix.setScale(x, y)
            val path = PathParser.createPathFromPathData("m0 16h32")
            path.transform(scaleMatrix)
            val pm = PathMeasure(path, true)
            val speed = 10000 / pm.length / speedMultiplier
            (length * speed).toInt()
        } catch (e: Exception) {
            4000
        }
    }

    /**
     * If you set the SVG data paths more than once using [.setGlyphStrings] you should call this method
     * before playing the animation.
     */
    fun rebuildGlyphData() {
        mTraceTime = 0
        var traceStartDelay = 0
        val x = mWidth / mViewport.x
        val y = mHeight / mViewport.y
        val scaleMatrix = Matrix()
        scaleMatrix.setScale(x, y)
        mGlyphData = Array(mGlyphStrings.size) { GlyphData() }
        for (i in mGlyphStrings.indices) {
            mGlyphData[i] = GlyphData()
            try {
                mGlyphData[i].path = PathParser.createPathFromPathData(
                    mGlyphStrings[i]
                )
                mGlyphData[i].path!!.transform(scaleMatrix)
            } catch (e: Exception) {
                mGlyphData[i].path = Path()
                Log.e(TAG, "Couldn't parse path", e)
            }
            val pm = PathMeasure(mGlyphData[i].path, false)
            do {
                mGlyphData[i].length = mGlyphData[i].length.coerceAtLeast(pm.length)
            } while (pm.nextContour())

            val traceTime = calculateTraceTime(mGlyphData[i].length)


            mTraceTime += traceTime
            mGlyphData[i].traceTime = traceTime
            mGlyphData[i].traceStartDelay = traceStartDelay
            traceStartDelay += traceTime + delayBetweenPaths
            mGlyphData[i].paint = Paint()
            mGlyphData[i].paint!!.style = Paint.Style.STROKE
            mGlyphData[i].paint!!.isAntiAlias = true
            mGlyphData[i].paint!!.color = Color.WHITE
            mGlyphData[i].paint!!.strokeCap = Paint.Cap.ROUND
            mGlyphData[i].paint!!.strokeJoin = Paint.Join.ROUND
            mGlyphData[i].paint!!.strokeWidth =
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    strokeWidth,
                    resources.displayMetrics
                )
        }
        mTraceTime += delayBetweenPaths * (mGlyphData.size - 1)
    }

    /**
     * Set the viewport width and height of the SVG. This can be found in the viewBox in the SVG. This is not the size
     * of the view.
     *
     * @param viewportWidth
     * the width
     * @param viewportHeight
     * the height
     */
    fun setViewportSize(viewportWidth: Float, viewportHeight: Float) {
        mViewportWidth = viewportWidth
        mViewportHeight = viewportHeight
        aspectRatioWidth = viewportWidth
        aspectRatioHeight = viewportHeight
        mViewport = PointF(mViewportWidth, mViewportHeight)
        requestLayout()
    }

    /**
     * Set the SVG path data.
     *
     * @param glyphStrings
     * The path strings found in the SVG.
     */
    fun setGlyphStrings(glyphStrings: Array<String>) {
        mGlyphStrings = glyphStrings
    }

    /**
     * Set the colors used during tracing the SVG
     *
     * @param traceResidueColors
     * the colors. Should be the same length as the SVG paths.
     */
    fun setStrokeColors(traceResidueColors: IntArray) {
        mTraceResidueColors = traceResidueColors
    }

    /**
     * Set the animation trace time
     *
     * @param traceTime
     * time in milliseconds
     */
    fun setTraceTime(traceTime: Int) {
        mTraceTime = traceTime
    }

    /**
     * Start the animation
     */
    fun start() {
        mStartTime = System.currentTimeMillis()
        changeState(STATE_TRACE_STARTED)
        ViewCompat.postInvalidateOnAnimation(this)
    }

    /**
     * Reset the animation
     */
    fun reset() {
        mStartTime = 0
        changeState(STATE_NOT_STARTED)
        ViewCompat.postInvalidateOnAnimation(this)
    }

    /**
     * Draw the SVG, skipping any animation.
     */
    fun setToFinishedFrame() {
        mStartTime = 1
        changeState(STATE_FINISHED)
        ViewCompat.postInvalidateOnAnimation(this)
    }

    /**
     * Get notified about the animation states.
     *
     * @param onStateChangeListener
     * The [OnStateChangeListener]
     */
    fun setOnStateChangeListener(onStateChangeListener: OnStateChangeListener?) {
        mOnStateChangeListener = onStateChangeListener
    }

    private fun changeState(@State state: Int) {
        if (this.state == state) return
        this.state = state
        mOnStateChangeListener?.onStateChange(state)
    }

    /**
     * Callback for listening to animation state changes
     */
    interface OnStateChangeListener {
        /**
         * Called when the animation state changes.
         *
         * @param state
         * The state of the animation.
         * Either {[.STATE_NOT_STARTED],
         * [.STATE_TRACE_STARTED]},
         * [.STATE_FILL_STARTED] or
         * [.STATE_FINISHED]
         */
        fun onStateChange(@State state: Int)
    }

    @IntDef(STATE_NOT_STARTED, STATE_TRACE_STARTED, STATE_FINISHED)
    annotation class State
    internal class GlyphData {
        var path: Path? = null
        var paint: Paint? = null
        var length = 0f
        var traceTime = 0
        var traceStartDelay = 0
    }

    companion object {
        /** The animation has been reset or hasn't started yet.  */
        const val STATE_NOT_STARTED = 0

        /** The SVG is being traced  */
        const val STATE_TRACE_STARTED = 1

        /** The animation has finished  */
        const val STATE_FINISHED = 2
        private const val TAG = "SvgPathPlayer"
        private val INTERPOLATOR: Interpolator = LinearInterpolator()
        private fun constrain(v: Float): Float = 0F.coerceAtLeast(1F.coerceAtMost(v))
    }
}